package ai.protoaone.poros.address;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/address")
public class AddressController {
    private static final Logger logger = LoggerFactory.getLogger(AddressController.class);
    
    @Autowired
    private AddressService service;

    @PostMapping
    public AddressEntity create(@RequestBody AddressEntity AddressEntity) {
        logger.info("Creating");
        AddressEntity rv = service.create(AddressEntity);
        return rv;
    }

    @GetMapping("/{id}")
    public AddressEntity read(@PathVariable Long id) {
        logger.info("Reading. id{}",id);
        AddressEntity rv = service.read(id);
        return rv;
    }

    @PutMapping("/{id}")
    public AddressEntity update(
        @PathVariable Long id,
        @RequestBody AddressEntity AddressEntity
    ) {
        logger.info("Updating. id{}",id);
        AddressEntity rv = service.update(id, AddressEntity);
        return rv;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        logger.info("Deleting. id{}",id);
        service.delete(id);
    }
}
