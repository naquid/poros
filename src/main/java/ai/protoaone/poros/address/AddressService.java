package ai.protoaone.poros.address;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ai.protoaone.poros.contact.ContactEntity;
import ai.protoaone.poros.contact.ContactRepository;
import ai.protoaone.poros.contactType.ContactTypeEntity;

@Service
public class AddressService {
    private static final Logger logger = LoggerFactory.getLogger(AddressService.class);

    @Autowired
    private AddressRepository repository;
    @Autowired
    private ContactRepository contactRepository;

    public AddressEntity create(AddressEntity addressEntity) {
        return repository.save(addressEntity);
    }
    // public AddressEntity create(AddressEntity addressEntity) {
    //     ContactEntity contactEntity = ContactEntity.builder()
    //     .contactType(
    //         ContactTypeEntity.builder().contactTypeId("ADDRESS").build()
    //     )
    //     .value(null)
    //     .address(
    //         // AddressEntity.builder().line1(addressEntity.getLine1()).line2(addressEntity.getLine2()).build()
    //         // new AddressEntity(addressEntity.getLine1(), addressEntity.getLine2())
    //     )
    //     .build();
    //     contactRepository.save(contactEntity);
    //     // AddressEntity rv = repository.save(AddressEntity);
    //     return null;
    // }

    public AddressEntity read(Long id) {
        logger.info("id:{}",id);
        Optional<AddressEntity> entity = repository.findById(id);
        if(entity.isEmpty()) {
            throw new IllegalArgumentException("Entity Doesn't exist");
        }
        return entity.get();
    }

    public AddressEntity update(Long id, AddressEntity AddressEntity) {
        Optional<AddressEntity> existing = repository.findById(id);
        if(existing.isEmpty()) {
            throw new IllegalArgumentException("Entity Doesn't exist");
        }
        AddressEntity rv = repository.save(AddressEntity);
        return rv;
    }

    public void delete(Long id) {
        Optional<AddressEntity> existing = repository.findById(id);
        if(existing.isEmpty()) {
            throw new IllegalArgumentException("Entity Doesn't exist");
        }
        repository.delete(existing.get());
    }
}
