package ai.protoaone.poros.address;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ai.protoaone.poros.contact.ContactEntity;
// import ai.protoaone.poros.contactType.AddressTypeEntity;
import ai.protoaone.poros.organisation.OrganisationEntity;
import jakarta.persistence.CascadeType;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.MapsId;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity(name = "address")
public class AddressEntity {
    @Id
    @GeneratedValue
    private Long addressId;
    
    private String line1;
    private String line2;

    @JsonIgnore
    @OneToOne(
        cascade = CascadeType.ALL
    )
    // @MapsId
    @JoinColumn(name = "contact_id")
    private ContactEntity contact;

    // public AddressEntity(
    //     String line1,
    //     String line2
    // ) {
    //     this.line1 = line1;
    //     this.line2 = line2;
    // }
    
    // @JsonIgnore
    // @ManyToMany(
    //     // fetch = FetchType.EAGER,
    //     mappedBy = "contacts"
    // )
    // private Set<OrganisationEntity> organisations;

    // @JsonIgnore
    // @ManyToOne(
    //     fetch = FetchType.EAGER
    // )
    // @JoinColumn(name = "contact_type_id")
    // private AddressTypeEntity contactType;
}
