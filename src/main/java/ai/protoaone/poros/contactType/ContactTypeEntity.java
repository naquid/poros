package ai.protoaone.poros.contactType;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ai.protoaone.poros.contact.ContactEntity;
import ai.protoaone.poros.organisation.OrganisationEntity;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity(name = "contact_type")
public class ContactTypeEntity {
    @Id
    private String contactTypeId;
    
    private String name;
    
    @JsonIgnore
    @OneToMany(
        mappedBy = "contactType"
        , cascade = CascadeType.ALL
        , orphanRemoval = true
        , fetch = FetchType.LAZY
    )
    private Set<ContactEntity> contactType;
}
