package ai.protoaone.poros.contactType;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/contactType")
public class ContactTypeController {
    private static final Logger logger = LoggerFactory.getLogger(ContactTypeController.class);
    
    @Autowired
    private ContactTypeService service;

    @PostMapping
    public ContactTypeEntity create(@RequestBody ContactTypeEntity ContactTypeEntity) {
        logger.info("Creating");
        ContactTypeEntity rv = service.create(ContactTypeEntity);
        return rv;
    }

    @PostMapping("/bulk")
    public List<ContactTypeEntity> createBulk(@RequestBody List<ContactTypeEntity> ContactTypeEntity) {
        logger.info("Creating");
        List<ContactTypeEntity> rv = service.createBulk(ContactTypeEntity);
        return rv;
    }

    @GetMapping("/{id}")
    public ContactTypeEntity read(@PathVariable String id) {
        logger.info("Reading. id{}",id);
        ContactTypeEntity rv = service.read(id);
        return rv;
    }

    @PutMapping("/{id}")
    public ContactTypeEntity update(
        @PathVariable String id,
        @RequestBody ContactTypeEntity ContactTypeEntity
    ) {
        logger.info("Updating. id{}",id);
        ContactTypeEntity rv = service.update(id, ContactTypeEntity);
        return rv;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable String id) {
        logger.info("Deleting. id{}",id);
        service.delete(id);
    }
}
