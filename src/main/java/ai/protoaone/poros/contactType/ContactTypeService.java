package ai.protoaone.poros.contactType;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContactTypeService {
    private static final Logger logger = LoggerFactory.getLogger(ContactTypeService.class);

    @Autowired
    private ContactTypeRepository repository;

    public List<ContactTypeEntity> createBulk(List<ContactTypeEntity> dtos) {
        List<ContactTypeEntity> entities = dtos.stream().map(dto->this.create(dto)).collect(Collectors.toList());
        return entities;
    }

    public ContactTypeEntity create(ContactTypeEntity ContactTypeEntity) {
        ContactTypeEntity rv = repository.save(ContactTypeEntity);
        return rv;
    }

    public ContactTypeEntity read(String id) {
        logger.info("id:{}",id);
        Optional<ContactTypeEntity> entity = repository.findById(id);
        if(entity.isEmpty()) {
            throw new IllegalArgumentException("Entity Doesn't exist");
        }
        return entity.get();
    }

    public ContactTypeEntity update(String id, ContactTypeEntity ContactTypeEntity) {
        Optional<ContactTypeEntity> existing = repository.findById(id);
        if(existing.isEmpty()) {
            throw new IllegalArgumentException("Entity Doesn't exist");
        }
        ContactTypeEntity rv = repository.save(ContactTypeEntity);
        return rv;
    }

    public void delete(String id) {
        Optional<ContactTypeEntity> existing = repository.findById(id);
        if(existing.isEmpty()) {
            throw new IllegalArgumentException("Entity Doesn't exist");
        }
        repository.delete(existing.get());
    }
}
