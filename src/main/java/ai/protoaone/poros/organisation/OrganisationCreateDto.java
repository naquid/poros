package ai.protoaone.poros.organisation;

import ai.protoaone.poros.contact.ContactEntity;
import ai.protoaone.poros.organisationRelation.OrganisationRelationEntity;
import ai.protoaone.poros.organisationType.OrganisationTypeEntity;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.ForeignKey;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
// @Entity(name = "org")
public class OrganisationCreateDto {
    // @Id
    // @GeneratedValue
    private Long orgId;
    
    private String name;

    @Builder.Default
    // @ManyToMany(
    //     fetch = FetchType.EAGER
    //     //remove me for solo
    //     , cascade = CascadeType.ALL
    // )
    // @JoinTable(
    //     name = "org_to_org_type_mapping",
    //     joinColumns = @JoinColumn(name = "org_id"),
    //     inverseJoinColumns = @JoinColumn(name = "org_type_id"),
    //     foreignKey = @ForeignKey(name="org_to_org_type_fk")
    // )
    private Set<OrganisationTypeEntity> orgTypes = Set.of();
    
    @Builder.Default
    // @ManyToMany(
    //     fetch = FetchType.EAGER
    //     , cascade = CascadeType.ALL
    // )
    // @JoinTable(
    //     name = "org_to_contact_mapping",
    //     joinColumns = @JoinColumn(name = "org_id"),
    //     inverseJoinColumns = @JoinColumn(name = "contact_id"),
    //     foreignKey = @ForeignKey(name="org_to_contact_fk")
    // )
    private Set<ContactEntity> contacts = Set.of();
    
    // @JsonManagedReference
    // @OneToMany(
    //     mappedBy = "parentOrg",
    //     cascade = CascadeType.ALL,
    //     fetch = FetchType.LAZY
    // )
    // private Set<OrganisationRelation> children;
    
    // @JsonBackReference
    // @OneToMany(
    //     mappedBy = "childOrg",
    //     cascade = CascadeType.ALL,
    //     fetch = FetchType.LAZY
    // )
    // private Set<OrganisationRelation> parents;
}
