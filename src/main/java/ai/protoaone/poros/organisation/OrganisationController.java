package ai.protoaone.poros.organisation;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/org")
public class OrganisationController {
    private static final Logger logger = LoggerFactory.getLogger(OrganisationController.class);
    
    @Autowired
    private OrganisationService service;

    @PostMapping
    public OrganisationEntity create(@RequestBody OrganisationCreateDto organisationEntity) {
        logger.info("Creating");
        OrganisationEntity rv = service.create(organisationEntity);
        return rv;
    }

    @PostMapping("/bulk")
    public List<OrganisationEntity> createBulk(@RequestBody List<OrganisationCreateDto> organisationEntity) {
        logger.info("Creating");
        List<OrganisationEntity> rv = service.createBulk(organisationEntity);
        return rv;
    }

    @GetMapping("/{id}")
    public OrganisationEntity read(@PathVariable Long id) {
        logger.info("Reading");
        OrganisationEntity rv = service.read(id);
        return rv;
    }

    @PutMapping("/{id}")
    public OrganisationEntity update(
        @PathVariable Long id,
        @RequestBody OrganisationEntity organisationEntity
    ) {
        logger.info("Updating");
        OrganisationEntity rv = service.update(id, organisationEntity);
        return rv;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        logger.info("Deleting");
        service.delete(id);
    }
}
