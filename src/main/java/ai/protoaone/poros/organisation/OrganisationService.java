package ai.protoaone.poros.organisation;

import java.util.Optional;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ai.protoaone.poros.address.AddressEntity;
import ai.protoaone.poros.address.AddressRepository;
import ai.protoaone.poros.contact.ContactEntity;
import ai.protoaone.poros.contact.ContactRepository;
import ai.protoaone.poros.contactType.ContactTypeEntity;
import ai.protoaone.poros.contactType.ContactTypeRepository;
import ai.protoaone.poros.organisationType.OrganisationTypeEntity;
import ai.protoaone.poros.organisationType.OrganisationTypeRepository;

@Service
public class OrganisationService {
    private static final Logger logger = LoggerFactory.getLogger(OrganisationService.class);

    @Autowired
    private OrganisationRepository repository;
    @Autowired
    private ContactRepository contactRepository;
    @Autowired
    private ContactTypeRepository contactTypeRepository;
    @Autowired
    private OrganisationTypeRepository organisationTypeRepository;
    @Autowired
    private AddressRepository addressRepository;

    public List<OrganisationEntity> createBulk(List<OrganisationCreateDto> dtos) {
        List<OrganisationEntity> entities = dtos.stream().map(dto->this.create(dto)).collect(Collectors.toList());
        return entities;
    }

    public OrganisationEntity create(OrganisationCreateDto dto) {
        OrganisationEntity entity = OrganisationEntity
        .builder()
        .name(dto.getName())
        .orgTypes(dto.getOrgTypes())
        .contacts(dto.getContacts())
        .build();
        logger.info("Create Full Enter");
    
        try {
            Set<ContactEntity> contacts = entity.getContacts().stream().map(contact -> {
                logger.info("Processing Contact: {}", contact);
    
                ContactTypeEntity contactType = contact.getContactType();
                if (contactType != null) {
                    logger.info("Handling Contact Type: {}", contactType.getContactTypeId());
    
                    if (!contactTypeRepository.existsById(contactType.getContactTypeId())) {
                        logger.info("Saving new Contact Type: {}", contactType.getContactTypeId());
                        contactType = contactTypeRepository.save(contactType);
                    } else {
                        logger.info("Contact Type exists, fetching: {}", contactType.getContactTypeId());
                        contactType = contactTypeRepository.findById(contactType.getContactTypeId()).orElse(contactType);
                    }
                    contact.setContactType(contactType);
                }
    
                ContactEntity savedContact = contactRepository.save(contact);
                logger.info("Contact saved with ID: {}", savedContact.getContactId());
    
                AddressEntity address = contact.getAddress();
                if (address != null) {
                    logger.info("Handling Address for Contact ID: {}", savedContact.getContactId());
    
                    if (address.getAddressId() == null || !addressRepository.existsById(address.getAddressId())) {
                        logger.info("Saving new Address");
                        address.setContact(savedContact);
                        AddressEntity savedAddress = addressRepository.save(address);
                        savedContact.setAddress(savedAddress);
                    } else {
                        logger.info("Address exists, fetching");
                        address = addressRepository.findById(address.getAddressId()).orElse(address);
                        address.setContact(savedContact); 
                    }
                }
    
                logger.info("Contact Processing Exit");
                return savedContact;
            }).collect(Collectors.toSet());
    
            entity.setContacts(contacts);
    
            Set<OrganisationTypeEntity> orgTypes = entity.getOrgTypes().stream().map(orgType -> {
                if (!organisationTypeRepository.existsById(orgType.getOrgTypeId())) {
                    logger.info("Saving new Organisation Type: {}", orgType.getOrgTypeId());
                    orgType = organisationTypeRepository.save(orgType);
                } else {
                    logger.info("Organisation Type exists, fetching: {}", orgType.getOrgTypeId());
                    orgType = organisationTypeRepository.findById(orgType.getOrgTypeId()).orElse(orgType);
                }
                return orgType;
            }).collect(Collectors.toSet());
    
            entity.setOrgTypes(orgTypes);
    
            logger.info("Saving Organisation Entity");
            OrganisationEntity savedOrganisation = repository.save(entity);
    
            logger.info("Create Full Exit");
            return savedOrganisation;
    
        } catch (Exception e) {
            logger.error("Error during createFull: {}", e.getMessage(), e);
            throw new RuntimeException("Error creating full organisation entity", e);
        }
    }
    

    public OrganisationEntity read(Long id) {
        Optional<OrganisationEntity> rv = repository.findById(id);
        if(rv.isEmpty()) {
            throw new IllegalArgumentException("Entity Doesn't Exist");
        }
        return rv.get();
    }

    public OrganisationEntity update(Long id, OrganisationEntity organisationEntity) {
        Optional<OrganisationEntity> existing = repository.findById(id);
        if(existing.isEmpty()) {
            throw new IllegalArgumentException("Entity Doesn't Exist");
        }
        OrganisationEntity rv = repository.save(organisationEntity);
        return rv;
    }

    public void delete(Long id) {
        Optional<OrganisationEntity> existing = repository.findById(id);
        if(existing.isEmpty()) {
            throw new IllegalArgumentException("Entity Doesn't Exist");
        }
        repository.delete(existing.get());
    }
}
