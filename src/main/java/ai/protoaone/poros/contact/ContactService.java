package ai.protoaone.poros.contact;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContactService {
    private static final Logger logger = LoggerFactory.getLogger(ContactService.class);

    @Autowired
    private ContactRepository repository;

    public ContactEntity create(ContactEntity ContactEntity) {
        ContactEntity rv = repository.save(ContactEntity);
        return rv;
    }

    public ContactEntity read(Long id) {
        logger.info("id:{}",id);
        Optional<ContactEntity> entity = repository.findById(id);
        if(entity.isEmpty()) {
            throw new IllegalArgumentException("Entity Doesn't exist");
        }
        return entity.get();
    }

    public ContactEntity update(Long id, ContactEntity ContactEntity) {
        Optional<ContactEntity> existing = repository.findById(id);
        if(existing.isEmpty()) {
            throw new IllegalArgumentException("Entity Doesn't exist");
        }
        ContactEntity rv = repository.save(ContactEntity);
        return rv;
    }

    public void delete(Long id) {
        Optional<ContactEntity> existing = repository.findById(id);
        if(existing.isEmpty()) {
            throw new IllegalArgumentException("Entity Doesn't exist");
        }
        repository.delete(existing.get());
    }
}
