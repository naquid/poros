package ai.protoaone.poros.contact;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ai.protoaone.poros.address.AddressEntity;
import ai.protoaone.poros.contactType.ContactTypeEntity;
import ai.protoaone.poros.organisation.OrganisationEntity;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity(name = "contact")
public class ContactEntity {
    @Id
    @GeneratedValue
    private Long contactId;
    
    private String value;
    
    @Builder.Default
    @JsonIgnore
    @ManyToMany(
        // fetch = FetchType.EAGER,
        mappedBy = "contacts"
        , cascade = CascadeType.ALL
    )
    private Set<OrganisationEntity> organisations = Set.of();

    // @JsonIgnore
    @ManyToOne(
        fetch = FetchType.EAGER
        , cascade = CascadeType.ALL
    )
    @JoinColumn(name = "contact_type_id")
    private ContactTypeEntity contactType;

    @OneToOne(
        mappedBy = "contact"
        , cascade = CascadeType.ALL
        , fetch = FetchType.EAGER
        , optional = false
    )
    private AddressEntity address;
}
