package ai.protoaone.poros.contact;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/contact")
public class ContactController {
    private static final Logger logger = LoggerFactory.getLogger(ContactController.class);
    
    @Autowired
    private ContactService service;

    @PostMapping
    public ContactEntity create(@RequestBody ContactEntity ContactEntity) {
        logger.info("Creating");
        ContactEntity rv = service.create(ContactEntity);
        return rv;
    }

    @GetMapping("/{id}")
    public ContactEntity read(@PathVariable Long id) {
        logger.info("Reading. id{}",id);
        ContactEntity rv = service.read(id);
        return rv;
    }

    @PutMapping("/{id}")
    public ContactEntity update(
        @PathVariable Long id,
        @RequestBody ContactEntity ContactEntity
    ) {
        logger.info("Updating. id{}",id);
        ContactEntity rv = service.update(id, ContactEntity);
        return rv;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        logger.info("Deleting. id{}",id);
        service.delete(id);
    }
}
