package ai.protoaone.poros.organisationType;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/orgType")
public class OrganisationTypeController {
    private static final Logger logger = LoggerFactory.getLogger(OrganisationTypeController.class);
    
    @Autowired
    private OrganisationTypeService service;

    @PostMapping
    public OrganisationTypeEntity create(@RequestBody OrganisationTypeEntity organisationTypeEntity) {
        logger.info("Creating");
        OrganisationTypeEntity rv = service.create(organisationTypeEntity);
        return rv;
    }

    @PostMapping("/bulk")
    public List<OrganisationTypeEntity> createBulk(@RequestBody List<OrganisationTypeEntity> dtos) {
        logger.info("Creating");
        List<OrganisationTypeEntity> rv = service.createBulk(dtos);
        return rv;
    }

    @GetMapping("/{id}")
    public OrganisationTypeEntity read(@PathVariable String id) {
        logger.info("Reading. id{}",id);
        OrganisationTypeEntity rv = service.read(id);
        return rv;
    }

    @PutMapping("/{id}")
    public OrganisationTypeEntity update(
        @PathVariable String id,
        @RequestBody OrganisationTypeEntity OrganisationTypeEntity
    ) {
        logger.info("Updating. id{}",id);
        OrganisationTypeEntity rv = service.update(id, OrganisationTypeEntity);
        return rv;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable String id) {
        logger.info("Deleting. id{}",id);
        service.delete(id);
    }
}
