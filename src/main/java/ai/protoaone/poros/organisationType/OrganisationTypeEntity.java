package ai.protoaone.poros.organisationType;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import ai.protoaone.poros.organisation.OrganisationEntity;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(name = "org_type")
public class OrganisationTypeEntity {
    @Id
    private String orgTypeId;
    private String name;
    
    @JsonIgnore
    @ManyToMany(
        mappedBy = "orgTypes"
        , cascade = CascadeType.ALL
        //, fetch = FetchType.EAGER
    )
    private Set<OrganisationEntity> organisations;
}
