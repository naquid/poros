package ai.protoaone.poros.organisationType;

import java.util.Optional;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ai.protoaone.poros.organisation.OrganisationCreateDto;
import ai.protoaone.poros.organisation.OrganisationEntity;

@Service
public class OrganisationTypeService {
    private static final Logger logger = LoggerFactory.getLogger(OrganisationTypeService.class);

    @Autowired
    private OrganisationTypeRepository repository;

    public List<OrganisationTypeEntity> createBulk(List<OrganisationTypeEntity> dtos) {
        List<OrganisationTypeEntity> entities = dtos.stream().map(dto->this.create(dto)).collect(Collectors.toList());
        return entities;
    }

    public OrganisationTypeEntity create(OrganisationTypeEntity OrganisationTypeEntity) {
        OrganisationTypeEntity rv = repository.save(OrganisationTypeEntity);
        return rv;
    }

    public OrganisationTypeEntity read(String id) {
        logger.info("id:{}",id);
        Optional<OrganisationTypeEntity> entity = repository.findById(id);
        if(entity.isEmpty()) {
            throw new IllegalArgumentException("Entity Doesn't exist");
        }
        return entity.get();
    }

    public OrganisationTypeEntity update(String id, OrganisationTypeEntity OrganisationTypeEntity) {
        Optional<OrganisationTypeEntity> existing = repository.findById(id);
        if(existing.isEmpty()) {
            throw new IllegalArgumentException("Entity Doesn't exist");
        }
        OrganisationTypeEntity rv = repository.save(OrganisationTypeEntity);
        return rv;
    }

    public void delete(String id) {
        Optional<OrganisationTypeEntity> existing = repository.findById(id);
        if(existing.isEmpty()) {
            throw new IllegalArgumentException("Entity Doesn't exist");
        }
        repository.delete(existing.get());
    }
}
