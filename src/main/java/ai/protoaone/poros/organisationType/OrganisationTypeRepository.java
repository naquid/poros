package ai.protoaone.poros.organisationType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrganisationTypeRepository extends JpaRepository<OrganisationTypeEntity,String>{
    // @Query(value = """
    //         SELECT new ai.protoaone.poros.organisationType.OrganisationTypeDto()
    //         FROM OrganisationTypeEntity ot
    //         WHERE ot.orgTypeId = :orgTypeId
    //         """
    // )
    // public List<OrganisationTypeDto> get(String orgTypeId);
}
