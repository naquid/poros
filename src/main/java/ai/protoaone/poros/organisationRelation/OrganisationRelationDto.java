package ai.protoaone.poros.organisationRelation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class OrganisationRelationDto {
    private Long parentOrgId;
    private Long childOrgId;
    private String relation;
}
