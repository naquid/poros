package ai.protoaone.poros.organisationRelation;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import ai.protoaone.poros.organisation.OrganisationEntity;
import jakarta.annotation.Generated;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity(name = "org_to_org_relation")
public class OrganisationRelationEntity {
    @Id
    @GeneratedValue
    private Long orgRelId;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(
        name = "parent_org_id",
        referencedColumnName = "orgId"
    )
    private OrganisationEntity parentOrg;

    @JsonManagedReference
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(
        name = "child_org_id",
        referencedColumnName = "orgId"
    )
    private OrganisationEntity childOrg;

    private String relation;
}
