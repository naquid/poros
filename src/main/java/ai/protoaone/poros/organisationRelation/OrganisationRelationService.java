package ai.protoaone.poros.organisationRelation;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ai.protoaone.poros.organisation.OrganisationEntity;

@Service
public class OrganisationRelationService {
    private static final Logger logger = LoggerFactory.getLogger(OrganisationRelationService.class);

    @Autowired
    private OrganisationRelationRepository repository;

    public List<OrganisationRelationEntity> createBulk(List<OrganisationRelationDto> dtos) {
        List<OrganisationRelationEntity> entities = dtos.stream().map(dto->this.create(dto)).collect(Collectors.toList());
        return entities;
    }
    
    public OrganisationRelationEntity create(OrganisationRelationDto dto) {
        OrganisationRelationEntity entity = OrganisationRelationEntity
        .builder()
        .childOrg(OrganisationEntity
            .builder()
            .orgId(dto.getChildOrgId())
            .build()
        )
        .parentOrg(OrganisationEntity
            .builder()
            .orgId(dto.getParentOrgId())
            .build()
        )
        .relation(dto.getRelation())
        .build();
        OrganisationRelationEntity rv = repository.save(entity);
        return rv;
    }

    public OrganisationRelationEntity read(Long id) {
        logger.info("id:{}",id);
        Optional<OrganisationRelationEntity> entity = repository.findById(id);
        if(entity.isEmpty()) {
            throw new IllegalArgumentException("Entity Doesn't exist");
        }
        return entity.get();
    }

    public OrganisationRelationEntity update(Long id, OrganisationRelationEntity OrganisationRelationEntity) {
        Optional<OrganisationRelationEntity> existing = repository.findById(id);
        if(existing.isEmpty()) {
            throw new IllegalArgumentException("Entity Doesn't exist");
        }
        OrganisationRelationEntity rv = repository.save(OrganisationRelationEntity);
        return rv;
    }

    public void delete(Long id) {
        Optional<OrganisationRelationEntity> existing = repository.findById(id);
        if(existing.isEmpty()) {
            throw new IllegalArgumentException("Entity Doesn't exist");
        }
        repository.delete(existing.get());
    }
}
