package ai.protoaone.poros.organisationRelation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("/orgRelation")
public class OrganisationRelationController {
    private static final Logger logger = LoggerFactory.getLogger(OrganisationRelationController.class);
    
    @Autowired
    private OrganisationRelationService service;

    @PostMapping
    public OrganisationRelationEntity create(@RequestBody OrganisationRelationDto organisationRelationDto) {
        logger.info("Creating");
        OrganisationRelationEntity rv = service.create(organisationRelationDto);
        return rv;
    }

    @PostMapping("/bulk")
    public List<OrganisationRelationEntity> createBulk(@RequestBody List<OrganisationRelationDto> dtos) {
        logger.info("Creating");
        List<OrganisationRelationEntity> rv = service.createBulk(dtos);
        return rv;
    }

    @GetMapping("/{id}")
    public OrganisationRelationEntity read(@PathVariable Long id) {
        logger.info("Reading. id{}",id);
        OrganisationRelationEntity rv = service.read(id);
        return rv;
    }

    @PutMapping("/{id}")
    public OrganisationRelationEntity update(
        @PathVariable Long id,
        @RequestBody OrganisationRelationEntity OrganisationRelationEntity
    ) {
        logger.info("Updating. id{}",id);
        OrganisationRelationEntity rv = service.update(id, OrganisationRelationEntity);
        return rv;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        logger.info("Deleting. id{}",id);
        service.delete(id);
    }
}
