package ai.protoaone.poros.organisationRelation;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrganisationRelationRepository extends JpaRepository<OrganisationRelationEntity,Long>{
    
}
