package ai.protoaone.poros;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PorosApplication {

	public static void main(String[] args) {
		SpringApplication.run(PorosApplication.class, args);
	}

}
